# Einführung in R für die Statistik Veranstaltung

Dieses Repo dient als zentrale Versionskontrolle des Latex-, Markdown- und R-Codes.

Bei Fragen oder Anmerkungen bitte einfach ein <s> Ticket/Issue erstellen</s> oder direkt eine Mail an: 

<a href="mailto:rene-marcel.kruse@uni-goettingen.de" title="Opens window for sending email" class="mail">rene-marcel.kruse[at]uni-goettingen.de</a>

## Aufgabe

Es ist Ziel eine neue Einführung in R für den Bachelor Kurs "Statistik" der Wirtschaftswissenschaftlen aufzubauen.

## Struktur

Der aktuelle Stand des Materials immer im File "rabasics.pdf" zu finden.

### Literatur

Bücher Liste:
- [A Handbook of statistical analysis using R](https://www.crcpress.com/A-Handbook-of-Statistical-Analyses-using-R/Hothorn-Everitt/p/book/9781482204582) von Everitt, Hothorn
- [Advanced R](https://adv-r.hadley.nz/) von Wickham
- [Practical Data Science with R](https://www.manning.com/books/practical-data-science-with-r) von Zumel & Mount 
- [R Cookbook](https://www.oreilly.com/library/view/r-cookbook-2nd/9781492040675/) von Teetor
- [The Art of R Programming](https://www.oreilly.com/library/view/the-art-of/9781593273842/) von Matloff
- [The Book of R](https://nostarch.com/bookofr) von Davies
- [The R Software](https://www.springer.com/de/book/9781461490197) de Micheaux, Drouilhet & Liquet 




Web:

- https://cran.r-project.org/doc/manuals/r-release/R-intro.pdf
- https://cran.r-project.org/doc/manuals/r-release/R-lang.pdf

