# Einleitung und Vorwort

Diese Skript ist als Teil der Bachelor Veranstaltung „Statistik“ der Wirtschaftswissenschaftlichen Fakultät der Universität Göttingen entstanden mit dem Ziel der Studierendenschaft eine einfache und übersichtliche Einführung in die Wissenschaftliche Programmierung anhand der Programmiersprache **R** zu geben.

In den folgenden Kapiteln wirst man unteranderem folgende Dinge lernen: ...

- wie man **R** und RStudio [installiert](#Vorbereitung) und [nutzt](#grundlagen)
- wie man Daten in **R** 
  - [einlesen](#einlesen) 
  - [aufarbeiten](#datawrangling)
  - [speichern](#einlesen) kann
- wie man [**R**-Packages](#rpackages) findet und nutzt
- wie man Funktionen/Programme in **R** [schreibt](#rfunktion) 
- wie man [Visualisierungen](#rgraphic) erstellt
- wie man Erkenntnise darzustellen und kommunizieren kann
- wie man erste [Daten Analyse und Inferenz](#inferenz) betreiben kann 

Für die Bearbeitung der Aufgaben und der Bearbeitung der gegebenen Beispiele benötigst man nicht mehr als einen Computer bzw. Zugang zu einem web-basierten RStudio Cliet (wenn man noch nicht weißt was das ist, keine Angst das folgende Kapitel wird dir eine Einführung geben) und vor allem Lust und Zeit etwas neues zu lernen.

<!-- TODO: Übersicht auf was noch kommt, sowie Lernpfade für verschiedene Leser einbauen -->

## Lernpfade

Dieses Skript besteht aus verschiedenen Kapiteln, wobei je nach Interesse und Hintergrund des Lesers es verschiedene vorgeschlagene Lesepfade existieren.

### Pfad 1: **R** Novizen
(**R**) Programmierungs Unerfahrene und Studierende des Moduls "Statistik" sind dazu angehalten sich durch alle Kapitel des Skriptes zu arbeiten. Einizig Kapitel welche als "Exkurs" gekennzeichnet sind, sich als freiwillige Lektüre zu sehen und bieten interessierten Lesern mehr Information und Einsichten in die Grundlagen der **R** Programmierung.

### Pfad 2: **R** als Taschenrechner
Für Leser welche sich mit den Fähigkeiten von **R** als Taschenrechnerersatz auseinander setzen wollen, sei es für arithmetische Operationen, Berechnung von Größen der Deskriptiven Statistik oder anderer Maßzahlen, sein auf das [Kapitel 3.3](#basicfunktion) hingewiesen.

### Pfad 3: **R** für emprische Arbeiten
Leser welche an einem eigenem Projekt sitzen, sei es nun eine Seminar- oder Abschlussarbeit, sein vor allem auf die [Kapitel 4](#datawrangling) zur Daten bearbeitung, [Kapitel 5](#rgraphic) zur graphischen Analyse und Darstellung und auf das und auf [Kapitel 6](#inferenz) zur Inferenz-basierten Arbeit hingewiesen.